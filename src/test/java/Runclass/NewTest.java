package Runclass;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import calculating.calculating;
import io.appium.java_client.windows.WindowsDriver;

public class NewTest {
	private static WindowsDriver CalculatorSession = null;
	private static WebElement CalculatorResult = null;
	static calculating cal = new calculating();

	@BeforeClass
	public static void setup() {
		cal.setup();
	}

	@AfterClass
	public static void TearDown() {
		cal.TearDown();
	}

	@Test
	public void f() throws Exception {
		cal.Addition();

	}

	protected String _GetCalculatorResultText() {
		// trim extra text and whitespace off of the display value
		return CalculatorResult.getText().replace("Display is", "").trim();

	}


}
