package report;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.appium.java_client.windows.WindowsDriver;

public class report {
	public ExtentReports extentReport() {

		ExtentReports extent = new ExtentReports();
		String Report;
		Report = "./repor/ReportSS.html";
		
		String Picture = ".r-img{width:50%}";

		ExtentHtmlReporter htmlreport = new ExtentHtmlReporter(Report);
		htmlreport.config().setCSS(Picture);
		htmlreport.config().setTheme(Theme.DARK);
		
		//htmlreport.setAppendExisting(true);
		
		extent.attachReporter(htmlreport);
		

		return extent;
	}// end of method

	public ExtentTest createTest(ExtentReports extent) {

		ExtentTest test = extent.createTest("MyFirstTest");

		return test;

	}// end method

	public String takeScreenShot(WindowsDriver CalculatorSession ) {

		String fileName = "pic";
		try {
			File srcFile = ((TakesScreenshot) (CalculatorSession )).getScreenshotAs(OutputType.FILE);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			fileName = ".\\screenshot\\" + fileName + timeStamp + ".png";

			// copying screeshot to directory
			FileUtils.copyFile(srcFile, new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileName;

	}// end of method



}
