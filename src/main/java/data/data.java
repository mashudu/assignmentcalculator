package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class data {
	public int columnCount(Sheet sheet) throws Exception {
		return sheet.getRow(0).getLastCellNum();
	}

	public String getCellData(String strColumn, int iRow, Sheet sheet) throws Exception {
		String sValue = null;
		Row row = sheet.getRow(0);
		for (int i = 0; i < columnCount(sheet); i++) {
			if (row.getCell(i).getStringCellValue().trim().equals(strColumn)) {
				Row raw = sheet.getRow(iRow);
				Cell cell = raw.getCell(i);
				DataFormatter formater = new DataFormatter();
				sValue = formater.formatCellValue(cell);
			}
		}
		return sValue;
	}
	public  void writeXLSXFile(int row, int col, String results) throws IOException {
        try {
            FileInputStream file = new FileInputStream("datas\\cal.xlsx");

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Cell cell = null;

            //Retrieve the row and check for null
            XSSFRow sheetrow = sheet.getRow(row);
            if(sheetrow == null){
                sheetrow = sheet.createRow(row);
            }
            //Update the value of cell
            cell = sheetrow.getCell(col);
            if(cell == null){
                cell = sheetrow.createCell(col);
            }
            cell.setCellValue(results);

            file.close();

            FileOutputStream outFile =new FileOutputStream(new File("datas\\cal.xlsx"));
            workbook.write(outFile);
            outFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
