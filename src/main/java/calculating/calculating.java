package calculating;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;

import data.data;
import io.appium.java_client.windows.WindowsDriver;
import report.report;


public class calculating {

	private static WindowsDriver CalculatorSession = null;
	private static WebElement CalculatorResult = null;
	data d = new data();
report re=new report();
	public static void setup() {
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
			CalculatorSession = new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
			CalculatorSession.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

			CalculatorResult = CalculatorSession.findElementByAccessibilityId("CalculatorResults");
			Assert.assertNotNull(CalculatorResult);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public void Addition() throws Exception {
		File src = new File("datas\\calculator.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(src);
		XSSFSheet sheets = workbook.getSheet("Sheet1");
		int rowCount = sheets.getPhysicalNumberOfRows();
		for (int j = 1; j < rowCount; j++) {

			CalculatorSession.findElementByName(d.getCellData("No1", j, sheets)).click();
			CalculatorSession.findElementByName(d.getCellData("sign", j, sheets)).click();
			CalculatorSession.findElementByName(d.getCellData("No2", j, sheets)).click();
			CalculatorSession.findElementByName(d.getCellData("equalsign", j, sheets)).click();

			// Assert.assertEquals("6", _GetCalculatorResultText());

			ExtentReports extent = re.extentReport();

			ExtentTest test = re.createTest(extent);

			String screenshot = re.takeScreenShot(CalculatorSession);
			if (_GetCalculatorResultText().equals("6")) {

d.writeXLSXFile(2, 2, _GetCalculatorResultText());
				test.log(Status.PASS, "pass");
				test.pass("pass");
				test.pass("Sreenshot", MediaEntityBuilder.createScreenCaptureFromPath("." + screenshot).build());

			} else {
				test.log(Status.FAIL, "fail");
				test.fail("fail");
			}

			extent.flush();
		}

	}

	public static void TearDown() {
		CalculatorResult = null;
		if (CalculatorSession != null) {
			CalculatorSession.quit();
		}
		CalculatorSession = null;
	}

	protected String _GetCalculatorResultText() throws IOException {
		// trim extra text and whitespace off of the display value

		return CalculatorResult.getText().replace("Display is", "").trim();

	}

}
